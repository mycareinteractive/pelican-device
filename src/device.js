import Radio from 'backbone.radio';


var isCustomEventSupported = (
    'CustomEvent' in window &&
    // in Safari, typeof CustomEvent == 'object' but it works
    (typeof window.CustomEvent === 'function' ||
        (window.CustomEvent.toString().indexOf('CustomEventConstructor') > -1))
);

export default class Device {

    constructor() {
    }

    /**
     * Start bootstrap process
     */
    bootstrap() {
    }


    /**
     * Get device platform.
     * Device platform is the unique name for every platform. Eg: "DESKTOP", "ENSEO", "PROCENTRIC"
     * @return {string} Device platform
     */
    getPlatform() {
    }

    /**
     * Get hardware model.
     * @return {string}
     */
    getHardwareModel() {
    }

    /**
     * Get hardware version
     * @return {string}
     */
    getHardwareVersion() {
    }

    /**
     * Get hardware serial number
     * @return {string}
     */
    getHardwareSerial() {
    }

    /**
     * Get middleware version
     * @return {string}
     */
    getMiddlewareVersion() {
    }

    /**
     * Network status
     * @typedef {Device[]} NetworkStatus
     * @property {string} Device.name
     * @property {string} Device.status
     * @property {string} Device.mac
     * @property {string} Device.ip
     * @property {string} Device.gateway
     * @property {string} Device.netmask
     * @property {string} Device.mode "NOT_REACHABLE", "UNKNOWN", "WIRE" or "WIRELESS"
     */

    /**
     * Get all network devices
     * @return {Object}
     */
    getNetworkDevices() {
    }

    /**
     * Get power status
     * @return {string}
     */
    getPower() {
    }

    /**
     * Set power state
     * @param {boolean} state true for on, otherwise false
     */
    setPower(state) {
    }

    /**
     * Get volume level
     */
    getVolume() {
    }

    /**
     * Set volume level
     * @param {Number} level volume level 0-100
     */
    setVolume(level) {
    }

    /**
     * Play a tv channel
     * @param {string} param.mode - Tuning mode, "ChannelNumber" or "LogicalNumber"
     * @param {string} param.type - Channel type, "UDP", "Analog" or "Digital"
     * @param {string} param.rfType - RF broadcast type, "Cable" or "Air"
     * @param {number} param.majorNumber - RF channel number
     * @param {number} param.minorNumber - RF minor channel for digital signals
     * @param {string} param.ip - UDP ip address
     * @param {number} param.port - UDP port
     * @return {Object} jQuery promise
     */
    playChannel(param) {
    }

    /**
     * Stop current playing channel
     * @return {Object} jQuery promise
     */
    stopChannel() {
    }

    /**
     * Play a media stream
     * @param {string} param.url - Video URL
     * @return {Object} jQuery promise
     */
    playMedia(param) {
    }

    /**
     * Stop current playing media
     * @return {Object} jQuery promise
     */
    stopMedia() {
    }

    /**
     * Pause current playing media
     * @return {Object} jQuery promise
     */
    pauseMedia() {
    }

    /**
     * Resume current playing media
     * @return {Object} jQuery promise
     */
    resumeMedia() {
    }

    /**
     * Set media position
     * @return {Object} jQuery promise
     */
    setMediaPosition(param) {
    }

    /**
     * Get media position
     * @return {Object} jQuery promise
     */
    getMediaPosition() {
    }

    /**
     * Get media duration
     * @return {Object} jQuery promise
     */
    getMediaDuration() {
    }

    /**
     * Get media paused or not
     */
    getMediaPaused() {
    }

    /**
     * Set media muted or not
     * @param muted
     */
    setMediaMute(muted) {
    }

    /**
     * Set the video layer size
     * @param {Number} param.x - x position of the video
     * @param {Number} param.y - y position of the video
     * @param {Number} param.width - width of the video
     * @param {Number} param.height - height of the video
     */
    setVideoLayerSize(param) {
    }

    /**
     * Get the video layer size
     */
    getVideoLayerSize() {
    }

    /**
     * Display video layer
     */
    showVideoLayer() {
    }

    /**
     * Hide video layer
     */
    hideVideoLayer() {
    }

    /**
     * Set local time (for devices that does not uses NTP client)
     * @param {Object} param - Parameter object
     * @param {Date} param.date
     * @param {Number} param.year
     * @param {Number} param.month
     * @param {Number} param.day
     * @param {Number} param.hour
     * @param {Number} param.minute
     * @param {Number} param.second
     * @param {Number} param.gmtOffsetInMinute
     * @param {Number} param.isDaylightSaving
     */
    setTime(param) {
    }

    /**
     * Set local timezone
     */
    /**
     * Set local timezone
     * @param {string} param.name - Timezone name. eg: "Amercia/Los_Angles"
     * @param {string} param.posix - Timezone posix format. eg: "EST5EDT"
     */
    setTimezone(param) {
    }

    /**
     * Reload app
     * @param param
     */
    reload(param) {
    }

    /**
     * Reboot device
     * @param param
     */
    reboot(param) {
    }

    /**
     * Change input port
     * @param input
     */
    setExternalInput(input) {
    }

    /**
     * Get device input port
     */
    getExternalInput() {
    }

    // Device bootstrap events are not jQuery events because we want them to be earlier than DOM ready
    triggerDeviceReady() {
        if(isCustomEventSupported) {
            var e = new CustomEvent('deviceready');
            document.dispatchEvent(e);
        }
        else {
            $(document).trigger('deviceready');
        }
    }

    triggerDeviceProgress(d) {
        if(isCustomEventSupported) {
            var e = new CustomEvent('deviceprogress');
            e.data = d;
            document.dispatchEvent(e);
        }
        else {
            $(document).trigger('deviceprogress', d);
        }
    }

    triggerDeviceFail(d) {
        if(isCustomEventSupported) {
            var e = new CustomEvent('devicefail');
            e.data = d;
            document.dispatchEvent(e);
        }
        else {
            $(document).trigger('devicefail', d);
        }
    }

    triggerKey(e, key) {
        // send a Backbone Radio event through 'device' channel
        var event = new $.Event('key', key);
        var channel = Radio.channel('device');
        event.keyCode = e.which;
        event.keyName = key;
        channel.trigger('key', event, key);

        if (event.isDefaultPrevented()) {
            e.preventDefault();
        }
        if (event.isPropagationStopped()) {
            e.stopPropagation();
        }
        if (event.isImmediatePropagationStopped()) {
            e.stopImmediatePropagation();
        }
        e.handled = event.handled;
    }

    triggerMediaEvent(type) {
        var channel = Radio.channel('media');
        channel.trigger.apply(channel, arguments);
    }

    triggerChannelEvent(type) {
        var channel = Radio.channel('channel');
        channel.trigger.apply(channel, arguments);
    }
}

var KeyCodes = {
    Back: 'BACK',
    Close: 'CLOSE',
    Enter: 'ENTER',
    Exit: 'EXIT',
    Menu: 'MENU',
    Home: 'HOME',
    Pause: 'PAUSE',
    Escape: 'ESCAPE',
    PageUp: 'PGUP',
    PageDown: 'PGDN',
    Left: 'LEFT',
    Up: 'UP',
    Right: 'RIGHT',
    Down: 'DOWN',
    Num0: '0',
    Num1: '1',
    Num2: '2',
    Num3: '3',
    Num4: '4',
    Num5: '5',
    Num6: '6',
    Num7: '7',
    Num8: '8',
    Num9: '9',
    ChDown: 'CHDN',
    ChUp: 'CHUP',
    Rewind: 'RWND',
    Play: 'PLAY',
    Forward: 'FFWD',
    Stop: 'STOP',
    VolumeDown: 'VOLDN',
    VolumeUp: 'VOLUP',
    Mute: 'MUTE',
    Power: 'POWER',
    PowerOff: 'OFF',
    PowerOn: 'ON',
    CC: 'CC',

    Red: 'RED',
    Yellow: 'YELLOW',
    Blue: 'BLUE',
    Green: 'GREEN'
};

var MediaEvents = {
    Start: 'start',
    End: 'end',
    Paused: 'paused',
    SeekDone: 'seekDone',
    PlayError: 'playError',
    BufferFull: 'bufferFull',
    FileNotFound: 'fileNotFound',
    NetworkDisconnected: 'networkDisconnected',
    NetworkBusy: 'networkBusy',
    NetworkCannotProcess: 'networkCannotProcess'
};

export {KeyCodes};
export {MediaEvents};

